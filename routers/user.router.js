const express = require('express');
const route = express.Router();
var db = require('../db');

var controller = require('../controllers/user.controller');
route.get('/', controller.index);
route.get('/search', controller.search);
route.get('/create', function(req, res){
	res.render('users/create');
});
route.post('/create', controller.create);
route.get('/:id', controller.view );
module.exports = route;