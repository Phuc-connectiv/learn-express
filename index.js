const express = require('express');
const app = express();
const port = 8888;
var db = require('./db');
var route = require('./routers/user.router');
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.set('view engine', 'pug');
app.set('views','./views');

app.get('/', function(req, res){
  res.render('index', {
  	name: 'Phuc Bua Dai K'
  });

});
app.use(express.static('public'));//cho phep truy cap http://localhost:8888/image/icon.png trong public
app.use('/user', route);
app.listen(port, function(){
  console.log('server start with port',port);
});
